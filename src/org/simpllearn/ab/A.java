package org.simpllearn.ab;

//import static com.simpllearn.ab.A;

public class A {
	
	//accessible everywhere
	public static int publicV =5 ;
	//Default is accessible within a package
	static int defaultV =10 ;
	//protected is accessible within a class,package,outside the package with the help of inheritance
	protected static int protectedV=15;
	//private is accessible within a class only
	private static int privateV =20;
	
	
	private static void privatemethod() {
				System.out.println(privateV);};
	static void defaultmethod() {

		System.out.println(defaultV);};
	protected static int protectedmethod() {
		System.out.println(protectedV);
	return protectedV;};
	public static void publicmethod() {
		System.out.println(publicV);};
		
	}


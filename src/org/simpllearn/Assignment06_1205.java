package org.simpllearn;

public class Assignment06_1205 {
	
	public static void main(String[] args) {
		  //Variables Definition and Initialization
		  int num1 = 12, num2 = 4;

		  //is equal to
		  System.out.println("num1 == num2 => " + (num1 == num2) );

		  //is not equal to
		  System.out.println("num1 != num2 => " + (num1 != num2) );

		  //Greater than
		  System.out.println("num1 >  num2 => " + (num1 >  num2) );

		  //Less than
		  System.out.println("num1 <  num2 => " + (num1 <  num2) );

		  //Greater than or equal to
		  System.out.println("num1 >= num2 = " + (num1 >= num2) );

		  //Less than or equal to
		  System.out.println("num1 <= num2 => " + (num1 <= num2) );
		  
		  System.out.println("\n"+"\n");
		  
		// Using addition operator
	    	int sum = num1 + num2;
	    	System.out.println("sum: (num1 + num2) = " + sum);
	    	
	    	// Using subtraction operator
	    	int difference = num1 - num2;
	    	System.out.println("difference: (num1 - num2) = " + difference);
	    	
	    	// Using multiplication operator
	    	int product = num1 * num2;
	    	System.out.println("product: (num1 * num2) = " + product);
	    	// Using division operator
	    	float quotient = num1 / num2;
	    	System.out.println("quotient: (num1 / num2) = " + quotient);
	    	
	    	// Using remainder operator
	    	float remainder = num1 % num2;
	    	System.out.println("remainder: (num1 % num2) = " + remainder);

		 }
		
}

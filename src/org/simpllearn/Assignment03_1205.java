package org.simpllearn;

import java.util.Scanner;

public class Assignment03_1205 {
	public static void main(String[] args) {
	Scanner s = new Scanner(System.in);
	System.out.print("number of the month/name of the month:");
	String choice = s.next();
	switch(choice) {
	case "12":
	case "1":
	case "11":
	case "December":
	case "January":
	case "November":	
		System.out.println("Season is Winter!");
		break;
	
	case "2":
	case "3":
	case "4":
	case "February":
	case "March":
	case "April":
		System.out.println("Season is Spring!");
		break;
		
	case "8":
	case "9":
	case "10":
	case "August":
	case "September":
	case "October":
		System.out.println("Season is Rainy!");
		break;
		
	case "5":
	case "6":
	case "7":
	case "May":
	case "June":
	case "July":
		System.out.println("Season is Summer!");
		break;
	}
}
}